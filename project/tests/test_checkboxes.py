# import pytest
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys

# driver = webdriver.Chrome()

# driver.get("http://the-internet.herokuapp.com/checkboxes")

# checkboxes = driver.find_elements(By.CSS_SELECTOR, "input[type='checkbox']")

# if not checkboxes[0].is_selected():
#     checkboxes[0].click()

# if checkboxes[1].is_selected():
#     checkboxes[1].click()

# assert checkboxes[0].is_selected()
# assert not checkboxes[1].is_selected()

# driver.quit()

# tests/test_checkboxes.py

from pages.checkbox_page import CheckboxPage
import pytest

@pytest.fixture
def checkbox_page(driver):
    return CheckboxPage(driver)

def test_select_checkbox(checkbox_page):
    checkbox_page.navigate_to_checkboxes_page()

    # Select the first checkbox
    checkbox_page.select_checkbox(0)
    assert checkbox_page.is_checkbox_selected(0)

    # Deselect the second checkbox
    checkbox_page.select_checkbox(1)
    assert not checkbox_page.is_checkbox_selected(1)

