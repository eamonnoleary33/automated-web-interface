import pytest
from selenium import webdriver

@pytest.fixture(scope="function")
def driver():
    # Specify the path to ChromeDriver
    #chromedriver_path = "/opt/homebrew/bin/chromedriver"

    # Initialize WebDriver with the specified ChromeDriver path
    driver = webdriver.Chrome()
    yield driver
    driver.quit()
