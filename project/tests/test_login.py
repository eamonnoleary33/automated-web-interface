from pages.login_page import LoginPage

def test_login(driver):
    login_page = LoginPage(driver)
    login_page.navigate_to_login_page()
    login_page.login("username", "password")
    assert login_page.is_login_successful()
