# pages/checkbox_page.py

from selenium.webdriver.common.by import By

class CheckboxPage:
    def __init__(self, driver):
        self.driver = driver

    def navigate_to_checkboxes_page(self):
        self.driver.get("http://the-internet.herokuapp.com/checkboxes")

    def select_checkbox(self, index):
        checkboxes = self.driver.find_elements(By.CSS_SELECTOR, "input[type='checkbox']")
        checkboxes[index].click()

    def is_checkbox_selected(self, index):
        checkboxes = self.driver.find_elements(By.CSS_SELECTOR, "input[type='checkbox']")
        return checkboxes[index].is_selected()
