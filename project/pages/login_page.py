from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class LoginPage:
    def __init__(self, driver):
        self.driver = driver

    def navigate_to_login_page(self):
        self.driver.get("http://the-internet.herokuapp.com/login")

    def login(self, username, password):
        username_field = self.driver.find_element(By.ID, "username")
        username_field.send_keys(username)
        password_field = self.driver.find_element(By.ID, "password")
        password_field.send_keys(password)
        login_button = self.driver.find_element(By.CSS_SELECTOR, "button[type='submit']")
        login_button.click()

    def is_login_successful(self):
        try:
            # Check if the success message is displayed
            WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, ".flash.success"))
            )
            return True
        except:
            # If success message is not displayed, check if error message for invalid username is displayed
            try:
                error_message = self.driver.find_element(By.CSS_SELECTOR, ".flash.error")
                return "Your username is invalid!" in error_message.text
            except:
                return False
